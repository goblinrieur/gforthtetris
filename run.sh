#! /usr/bin/env bash

runner=$(which gforth-fast)

if [[ ${runner} != *"gforth-fast"* ]] ; then 
	 echo "Gforth interpreter not found"
	 exit 1
fi
trap '' SIGINT
$runner -W ./tetris.fs
clear 
trap SIGINT
tput cnorm
exit $?
