# TETRIS gforth version 

It is a classical tetris rules game, for gnu-linux color ASCII terminal emulators & compatibles TTY's.

It is now more or less gnu-forth 0.7.x & UNIX-like system dependant. _(few words are going to be deprecated soon)_

This isn't a problem but prehistoric ages are over ! 
 
```
       _   _ _       
__   _/ | / / |  ___ 
\ \ / / | | | | / __|
 \ V /| |_| | || (__ 
  \_/ |_(_)_|_(_)___|
                     
```
                       
# Work on it for fun 

I just want to understand it ; make few minor changes etc...

# changes done from initial fork

- [x] Adapt deeper pit for more challenging game
- [x] Add my own signature
- [x] Exit properly *no line covering on terminal by adding carriage return*
- [X] Add score saving

Wed May 31 12:05:10 PM CEST 2023

- [X] Colored yellow while dropping stone
- [X] Colored white while line update & appearing stone
- [X] Colored red title

Mon Aug 21 04:30:35 PM CEST 2023

- [X] Cursor is now enable/disable by true|false word
- [X] CTRL+C is now a *BOSS KEY* to fast exit
- [X] A run.sh loads it
- [X] Many comments for auto-doc were updated

# here is a view of it running 

![pic](./game.png)

# code is forth

with [gforth](https://www.gnu.org/software/gforth/) 
